from PIL import Image

from conversions import bin_to_str, rgb_to_hex

DELIMETER_VAL = "1111111111111110"

def decode(hex_value):
    if hex_value[-1] in "01":
        return hex_value[-1]
    else:
        return None

def retrieving_message(img_file):
    img = Image.open(img_file)
    final_bin = ''

    if img.mode in ('RGBA'):
        img = img.convert('RGBA')
        pixels = img.getdata()

        for pixel in pixels:
            # decrypting the pixels to get message in hexadecimal form
            hex_message = decode(rgb_to_hex(pixel[0], pixel[1], pixel[2]))
            if hex_message == None:
                pass
            else:
                final_bin = final_bin + hex_message

                """ checking the delimeter if the final 16 charachters are delimeter value, the pixels were encoded and
                decoded completely """

                if final_bin[-16:] == DELIMETER_VAL:
                    print("SUCCESSFULLY OBTAINED OUTPUT")

                    # returning message from binary string
                    return bin_to_str(final_bin[:-16])
        # edge case if message exceeded image pixels
        return bin_to_str(final_bin)

    return "Error couldn't retrieve! Incorrect image mode. Make sure it is in PNG format"

