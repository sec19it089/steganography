import base64
import io
import json
from PIL import Image
from flask import render_template, Flask, flash, Response, json, redirect, url_for
from flask import request
from encode import hiding_in_picture
from decode import retrieving_message
from conversions import str_to_bin, hex_to_rgb, rgb_to_hex


DELIMETER_VAL = "1111111111111110"


app = Flask(__name__)

if __name__ == ' __main__':
    app.debug = True
    app.run(host='0.0.0.0')


@app.route('/success/<val>')
def success(val):
   return 'Yaayy' + val

@app.route('/encode', methods=['POST'])
def encode():
    source_image = request.files.get('source')
    message = request.form.get('message')
    encoded = hiding_in_picture(source_image, message)
    return redirect(url_for('success', val=encoded))


@app.route('/decode', methods=['POST', 'GET'])
def decode():
    source_image = request.files.get('source')
    decoded = retrieving_message(source_image)

    return redirect(url_for('success', val=decoded))

@app.route('/')
def home():
    return render_template('home.html')


if __name__ == '__main__':
    app.run(debug=True)
