from PIL import Image

from conversions import str_to_bin, hex_to_rgb, rgb_to_hex

DELIMETER_VAL = "1111111111111110"

def encode(hex_value, digit):
    if hex_value[-1] in "012345":
        return hex_value[:-1] + digit
    else:
        return None

def hiding_in_picture(pic_file, message):
    img = Image.open(pic_file)
    binary_form = str_to_bin(message) + DELIMETER_VAL

    # converting image to red-blue-green-alpha form which is editable
    if img.mode in ('RGBA'):
        img = img.convert('RGBA')

        # obtaining image pixels
        pixels = img.getdata()

        # new data will append the new pixel values
        new_data, digit = [], 0

        for pixel in pixels:
            if digit < len(binary_form):
                # finding the new pixel values for new image
                new_pixel_val = encode(rgb_to_hex(pixel[0], pixel[1], pixel[2]), binary_form[digit])

                # if pixel val is not valid just append the pixel value as such
                if new_pixel_val == None:
                    new_data.append(pixel)
                # if new pixel value is valid change the original pictures pixels one by one
                else:
                    r, g, b = hex_to_rgb(new_pixel_val)
                    new_data.append((r, g, b, 255))
                    digit += 1
            else:
                new_data.append(pixel)

        # saving the new pixels in final image
        img.putdata(new_data)
        img.save(pic_file, "PNG")
        return "COMPLETED HIDING PROCESS!"
    return "Error couldn't hide! Incorrect image mode. Make sure it is in PNG format"
